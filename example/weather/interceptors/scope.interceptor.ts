import { Interceptor } from '../../../lib'
import { AxiosResponse } from 'axios'

/**
 * 局部拦截器测试
 */
export class ScopeInterceptor extends Interceptor {
    onResponseSucceed(response: AxiosResponse): AxiosResponse | Promise<AxiosResponse> {
        console.log('向返回参数注入自定义参数')
        return {
            ...response,
            data: {
                ...response.data,
                description: '这是由全局拦截器注入的自定义参数'
            }
        }
    }
}
