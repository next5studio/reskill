import { Interceptor } from '../../../lib'
import { AxiosRequestConfig } from 'axios'

/**
 * 全局拦截器测试
 */
export class GlobalInterceptor extends Interceptor {
    beforeRequest(config: AxiosRequestConfig): AxiosRequestConfig | Promise<AxiosRequestConfig> {
        console.log('向headers注入自定义header')
        return {
            ...config,
            headers: {
                ...config.headers,
                description: 'this is custom header'
            }
        }
    }
}
