import { Reskill } from '../../../lib'
import { ScopeInterceptor } from '../interceptors/scope.interceptor'

/**
 * 今日头条天气信息接口样例
 */
export class WeatherCase extends Reskill {
    /**
     * 获取天气信息
     * @param city 城市名称
     */
    async getWeather(city: String) {
        return this.get({
            url: 'stream/widget/local_weather/data/',
            params: {
                city
            }
        })
    }

    /**
     * 获取天气信息(添加局部拦截器)
     * @param city 城市名称
     */
    async getWeatherWithScopeInterceptor(city: String) {
        return this.use(ScopeInterceptor).get({
            url: 'stream/widget/local_weather/data/',
            params: {
                city
            }
        })
    }
}
