import { WeatherCase } from './cases/weather.case'
import { Reskill } from '../../lib'
import { GlobalInterceptor } from './interceptors/global.interceptor'

// 配置Reskill
Reskill.config({
    baseURL: 'https://www.toutiao.com'
}).use(GlobalInterceptor)

export async function runWeather() {
    // 执行操作
    const weather = new WeatherCase()
    console.log((await weather.getWeather('泉州')).data)
    console.log((await weather.getWeatherWithScopeInterceptor('泉州')).data)
}
