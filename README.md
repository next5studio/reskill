# @next5/reskill

`Reskill` 是一个基于axios、ddd思想进行设计的前端请求层框架。

## 缘起缘生

自从半年前我开始接触ddd思想，就被它深深的吸引了，ddd指引我思考了很多东西。

这些东西大致可以倾向于以下几点：

1. 不论技术还是架构、最终都是为业务服务的
   
2. 在传统开发的过程中，我们经常丢失了一些原本属于业务的点（例如业务中用例行为的语义特点）

3. 不论是前端还是后端，都是基于业务所分析出来的模型，在其基础之上进行开发的，因此模型是本源，是最重要的东西

4. 在一切设计都进行的很理想的前提下，前后端的数据传输模型（即dto）变化的可能性不大，因此它不应该成为系统维护的痛点

基于以上几点，我又产生了如下思考：

1. 如何利用好typescript的特性，让请求层面的代码提示更加友好

2. 能否将请求层与模型层合并并抽象成单独的一层，store只负责调用

3. 后端dto发生变更时，如何降低前端的维护难度

