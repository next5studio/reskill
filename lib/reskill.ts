import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import { IInterceptor } from './interceptor/contracts.interceptor'

/**
 * Reskill基类
 *
 * 为子类提供发送请求的能力
 */
export abstract class Reskill {
    private static baseConfig: AxiosRequestConfig
    private static globalInterceptors: IInterceptor[] = []

    private axiosInstance!: AxiosInstance
    private interceptors: IInterceptor[] = []

    /**
     * 设置基础配置
     * @param config 基础配置
     */
    static config(config: AxiosRequestConfig) {
        this.baseConfig = config
        return this
    }

    /**
     * 添加全局拦截器
     * @param Interceptor 拦截器类
     */
    static use<TInterceptor extends IInterceptor>(Interceptor: { new (): TInterceptor } | TInterceptor) {
        this.globalInterceptors.push(typeof Interceptor === 'object' ? Interceptor : new Interceptor())
        return this
    }

    /**
     * 添加局部拦截器
     * @param Interceptor 拦截器类
     */
    public use<TInterceptor extends IInterceptor>(Interceptor: { new (): TInterceptor } | TInterceptor) {
        this.interceptors.push(typeof Interceptor === 'object' ? Interceptor : new Interceptor())
        return this
    }

    /**
     * 发送get请求
     * @param config 请求配置
     * @protected
     */
    protected async get(config: AxiosRequestConfig) {
        return this.request({
            ...config,
            method: 'GET'
        })
    }

    /**
     * 发送post请求
     * @param config 请求配置
     * @protected
     */
    protected async post(config: AxiosRequestConfig) {
        return this.request({
            ...config,
            method: 'POST'
        })
    }

    /**
     * 发送put请求
     * @param config 请求配置
     * @protected
     */
    protected async put(config: AxiosRequestConfig) {
        return this.request({
            ...config,
            method: 'PUT'
        })
    }

    /**
     * 发送patch请求
     * @param config 请求配置
     * @protected
     */
    protected async patch(config: AxiosRequestConfig) {
        return this.request({
            ...config,
            method: 'PATCH'
        })
    }

    /**
     * 发送delete请求
     * @param config 请求配置
     * @protected
     */
    protected async delete(config: AxiosRequestConfig) {
        return this.request({
            ...config,
            method: 'DELETE'
        })
    }

    /**
     * 发起实际请求
     * @param config 请求配置
     * @private
     */
    private async request(config: AxiosRequestConfig) {
        this.axiosInstance = axios.create(Reskill.baseConfig)
        this.interceptors = Reskill.globalInterceptors.concat(this.interceptors)

        let interceptor
        while ((interceptor = this.interceptors.pop())) {
            this.axiosInstance.interceptors.request.use(interceptor.beforeRequest, interceptor.handleRequestError)
            this.axiosInstance.interceptors.response.use(interceptor.onResponseSucceed, interceptor.onResponseFailed)
        }

        return this.axiosInstance(config)
    }
}
