import { AxiosRequestConfig, AxiosResponse } from 'axios'

/**
 * 拦截器规约
 */
export interface IInterceptor {
    /**
     * 请求发出之前处理请求配置
     * @param config 请求配置
     */
    beforeRequest?: (config: AxiosRequestConfig) => AxiosRequestConfig | Promise<AxiosRequestConfig>

    /**
     * 处理请求错误
     * @param error 请求错误信息
     */
    handleRequestError?: (error: any) => any

    /**
     * 服务器响应请求成功
     * @param response 响应体
     */
    onResponseSucceed?: (response: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>

    /**
     * 服务器响应请求失败
     * @param error 响应错误信息
     */
    onResponseFailed?: (error: any) => any
}
