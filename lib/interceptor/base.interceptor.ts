import { IInterceptor } from './contracts.interceptor'
import { AxiosRequestConfig, AxiosResponse } from 'axios'

/**
 * 基类拦截器实现
 */
export abstract class Interceptor implements IInterceptor {
    /**
     * 请求发出前执行
     * @param config 请求配置
     */
    beforeRequest(config: AxiosRequestConfig): AxiosRequestConfig | Promise<AxiosRequestConfig> {
        return config
    }

    /**
     * 请求出现错误执行
     * @param error 错误信息
     */
    handleRequestError(error: any): any {
        throw error
    }

    /**
     * 服务器响应请求失败执行
     * @param error 错误信息
     */
    onResponseFailed(error: any): any {
        throw error
    }

    /**
     * 服务器响应请求成功执行
     * @param response 响应体
     */
    onResponseSucceed(response: AxiosResponse): AxiosResponse | Promise<AxiosResponse> {
        return response
    }
}
